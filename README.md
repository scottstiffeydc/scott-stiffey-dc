Dr. Scott Stiffey is widely recognized as a leader in the non-drug treatment of chronic pain syndromes, autoimmune imbalances, thyroid conditions, peripheral neuropathy, diabetes, knee problems, frozen shoulder syndrome and other chronic neurological and metabolic conditions for 17 years.

Address: 1 East Broadway, Suite C1, Columbia, MO 65203, USA

Phone: 573-607-2727
